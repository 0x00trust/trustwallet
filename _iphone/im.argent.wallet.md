---
wsId: argent
title: Argent – DeFi in a tap
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2022-04-08
version: 4.6.0
stars: 4.5
reviews: 1575
size: '129216512'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 

---

{% include copyFromAndroid.html %}