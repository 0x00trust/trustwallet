---
wsId: swipestoxNaga
title: 'NAGA: Invest in Forex & Stocks'
altTitle: 
authors:
- danny
appId: com.swipestox.app
appCountry: gb
idd: 1182702365
released: 2017-01-15
updated: 2022-04-09
version: 8.2.6
stars: 4.4
reviews: 1494
size: '143551488'
website: https://www.naga.com
repository: 
issue: 
icon: com.swipestox.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-10
signer: 
reviewArchive: 
twitter: nagainvesting
social:
- https://www.linkedin.com/company/nagainvesting
- https://www.facebook.com/nagainvesting

---

{% include copyFromAndroid.html %}
