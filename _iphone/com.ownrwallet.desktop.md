---
wsId: OWNR
title: OWNR crypto wallet for PC
altTitle: 
authors:
- leo
appId: com.ownrwallet.desktop
appCountry: 
idd: 1520395378
released: 2020-08-13
updated: 2022-04-09
version: 2.2.3
stars: 0
reviews: 0
size: '136895558'
website: https://ownrwallet.com
repository: 
issue: 
icon: com.ownrwallet.desktop.png
bugbounty: 
meta: ok
verdict: nosource
date: 2021-05-31
signer: 
reviewArchive: 
twitter: ownrwallet
social:
- https://www.facebook.com/ownrwallet
- https://www.reddit.com/r/ownrwallet

---

