---
wsId: 
title: Crypto Exchange Currency.com
altTitle: 
authors:
- danny
appId: com.currency.exchange.prod2
appCountry: by
idd: 1458917114
released: 2019-04-23
updated: 2022-04-06
version: 1.24.0
stars: 4.8
reviews: 2854
size: '63364096'
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-01-09
signer: 
reviewArchive: 
twitter: currencycom
social:
- https://www.facebook.com/currencycom
- https://www.reddit.com/r/currencycom

---

<!--
  According to the Android review, this app was falsely marked as wsId currencycominvesting and needs another close look.
-->
