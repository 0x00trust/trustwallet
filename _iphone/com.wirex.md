---
wsId: wirex
title: 'Wirex: Buy, Spend & Sell BTC'
altTitle: 
authors:
- danny
appId: com.wirex
appCountry: us
idd: 1090004654
released: 2016-03-22
updated: 2022-04-09
version: 3.30.50
stars: 3.9
reviews: 444
size: '99358720'
website: https://wirexapp.com/en
repository: 
issue: 
icon: com.wirex.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-10
signer: 
reviewArchive: 
twitter: wirexapp
social:
- https://www.linkedin.com/company/wirex-limited
- https://www.facebook.com/wirexapp

---

{% include copyFromAndroid.html %}
