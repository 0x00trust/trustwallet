---
wsId: vita
title: Vita Wallet
altTitle: 
authors:
- danny
appId: io.vitawallet.vitawallet
appCountry: cl
idd: 1486999955
released: 2019-11-15
updated: 2022-04-13
version: 4.1.2
stars: 4.5
reviews: 47
size: '36702208'
website: https://www.vitawallet.io
repository: 
issue: 
icon: io.vitawallet.vitawallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: vitawallet
social:
- https://www.linkedin.com/company/vita-wallet
- https://www.facebook.com/vitawallet

---

{% include copyFromAndroid.html %}
